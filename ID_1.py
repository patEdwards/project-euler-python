# Multiples of 3 and 5

# If we list all the natural numbers below 10 that are multiples of 3 or 5,
#  we get 3, 5, 6 and 9. The sum of these multiples is 23.
# Find the sum of all the multiples of 3 or 5 below 1000.

# 2 strategies: (1) Go through 0 to 999 and check if multiple, if so, add
# to list and sum at the end, or (2), multiply 3 by 1,2,3... until over 999 and
# then do same with 5, checking to see whether there it's a multiple of 3 
# before adding it.

print sum([k for k in range(3,1000) if (k%3 == 0 or k%5==0)])

