#!/usr/bin/env python
import time
import numpy
from numpy import *
### Problem 3 ###

# The prime factors of 13195 are 5, 7, 13 and 29.

# What is the largest prime factor of the number 600851475143 ?

### Solution ### 

N = 600851475143

def is_factor(factor,n):
    m = float(n)/float(factor)
    return int(m)== m

def is_prime(m):
    F = 2
    not_sure = True
    while not_sure:
        if is_factor(F,m):
            return 0
        F += 1
        if F > float(m)/2:
            return 1

t_i = time.time()

prime_list = []
found = False
p = 1
f = 0
while not found:
    f += 1
    if is_factor(f,N) and is_prime(f):
        print f
        p = p*f
    if p >= N:
        found = True
        print f
t_f = time.time()
t = t_f - t_i
print 'time = %.5f' % t
